package com.sasaj.mydailyselfie.model;

import android.graphics.Bitmap;
import android.net.Uri;

public class Selfie {
	
	private long id;
	private String title;
	private String date;
	private long width;
	private long height;
	private Bitmap thumbnail;
	private String fileLocation;
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	public long getWidth() {
		return width;
	}
	public void setWidth(long width) {
		this.width = width;
	}
	
	
	public long getHeight() {
		return height;
	}
	public void setHeight(long height) {
		this.height = height;
	}
	
	
	public Bitmap getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(Bitmap thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	

    /**
     * @return the textual representation of Selfie object.
     */
    @Override
    public String toString() {
        return "{" +
            "Id: "+ id + ", "+
            "Title: "+ title + ", "+
            "Date: "+ date + ", "+
            "width: "+ width + ", "+
            "height: " + height + ", "+ 
            "file location: "+ fileLocation+ ", " +
            "thumbnail: " + thumbnail + 
            "}";
    }
}
