package com.sasaj.mydailyselfie.utils;

import java.util.ArrayList;
import java.util.List;

import com.sasaj.mydailyselfie.R;
import com.sasaj.mydailyselfie.model.Selfie;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SelfieListAdapter extends BaseAdapter {

	private final String TAG = getClass().getSimpleName();
	private Context mContext;
	private List<Selfie> list;
	

	public SelfieListAdapter(Context context) {
		super();
		// TODO Auto-generated constructor stub
		mContext = context;
		list = new ArrayList<Selfie>();
	}
	
	public void setSelfiesList (List<Selfie> result){
		
		list = result;
		notifyDataSetChanged();
	}
	
	public List<Selfie> getSelfies (){
		return list;
	}

	public void add(Selfie item) {

		list.add(item);
		notifyDataSetChanged();

	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Selfie getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		 View item = convertView;
		    if (item == null) {
		        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		        item = inflater.inflate(R.layout.listitem, null);
		    }
		    
		  Selfie selfie = list.get(position);
		  
		  TextView title = (TextView) item.findViewById(R.id.tv_details);
		  ImageView image = (ImageView) item.findViewById(R.id.img_thumbnail);
		  
		  String filename = selfie.getTitle();
		  String [] parts = filename.split("_");
		  String newTitle = parts[3]+"."+parts[2]+"."+parts[1]+". at "+parts[4]+":"+parts[5];
		  
		  title.setText(newTitle);
		  image.setImageBitmap(selfie.getThumbnail());
		return item;
	}

}
