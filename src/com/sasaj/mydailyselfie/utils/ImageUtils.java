package com.sasaj.mydailyselfie.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sasaj.mydailyselfie.model.Selfie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;

public class ImageUtils {
	
	 // convert from bitmap to byte array
	 public static byte[] getBytes(Bitmap bitmap) {
	  ByteArrayOutputStream stream = new ByteArrayOutputStream();
	  bitmap.compress(CompressFormat.PNG, 0, stream);
	  return stream.toByteArray();
	 }

	  // convert from byte array to bitmap
	 public static Bitmap getPhoto(byte[] image) {
	  return BitmapFactory.decodeByteArray(image, 0, image.length);
	 }

	public static Selfie createSelfie(String path) {
		
		Selfie selfie = new Selfie();
		
		selfie.setTitle(path.substring(path.lastIndexOf("/")+1));
		
		selfie.setFileLocation(path);
		
		selfie.setDate(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

	    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    bmOptions.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(path, bmOptions);
	    int width = bmOptions.outWidth;
	    int height = bmOptions.outHeight;
	    
	    selfie.setWidth(width);
	    selfie.setHeight(height);
		
		Bitmap thumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), width/6, height/6);
		
		selfie.setThumbnail(thumbnail);
		
		return selfie;
	}
	
	 
}
