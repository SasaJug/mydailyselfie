package com.sasaj.mydailyselfie.activities;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.sasaj.mydailyselfie.R;
import com.sasaj.mydailyselfie.model.Selfie;
import com.sasaj.mydailyselfie.operations.SelfieOps;
import com.sasaj.mydailyselfie.utils.GenericActivity;
import com.sasaj.mydailyselfie.utils.SelfieListAdapter;


public class SelfieListActivity  extends GenericActivity<SelfieOps> {


	@Override
	public void onCreate(Bundle savedInstanceState) {


        
		setContentView(R.layout.main);
		
    	// Call super class for initialization/implementation.
        super.onCreate(savedInstanceState, SelfieOps.class);
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	    	Log.d(TAG,"onActivityResult()");
	    	switch (requestCode){
	    	
	    	case SelfieOps.REQUEST_TAKE_PHOTO: 
	    		Log.d(TAG,"about to call getOps().saveSelfie()");
	    		getOps().saveSelfie();
	    		break;
	    
	    	case SelfieOps.REQUEST_SHOW_PHOTO:
	    		Log.d(TAG,"about to call getOps().getList()");
	    		getOps().getList();
	    		break;

	    	}
	    }
	}
	

	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	
        int id = item.getItemId();
        if (id == R.id.photo) {
        	getOps().dispatchTakePictureIntent();
        }
        
        return super.onOptionsItemSelected(item);

    }

	


}
