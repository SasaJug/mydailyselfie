package com.sasaj.mydailyselfie.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.sasaj.mydailyselfie.R;
import com.sasaj.mydailyselfie.operations.SelfieDetailOps;
import com.sasaj.mydailyselfie.operations.SelfieOps;
import com.sasaj.mydailyselfie.utils.GenericActivity;

public class ShowSelfieActivity extends GenericActivity<SelfieDetailOps>{
	
	
    public static final String ACTION_VIDEO_DETAIL =
            "com.sasaj.mydailyselfie.ShowSelfieActivity";
    
	public static final String LOCATION = "location";
	public static final String ID = "id";


    
    @Override
	public void onCreate(Bundle savedInstanceState) {


        
		setContentView(R.layout.show_selfie);

        super.onCreate(savedInstanceState, SelfieDetailOps.class);
        
      
	}
    
    public void deleteSelfie(View view){
    	getOps().deleteSelfie();
    }
    
   /* public void filterSelfie(View view){
    	getOps().filterSelfie();
    }
*/
}
