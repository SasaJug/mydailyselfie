package com.sasaj.mydailyselfie.provider;

import java.util.ArrayList;
import java.util.List;

import com.sasaj.mydailyselfie.model.Selfie;
import com.sasaj.mydailyselfie.provider.SelfieContract.SelfieEntry;
import com.sasaj.mydailyselfie.utils.ImageUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;


public class SelfieController {
	
	private final String TAG = getClass().getSimpleName();
	private Context mContext;
	
	public SelfieController (Context context){
		this.mContext =context;
	}
	
	/**
	 * gets all rows from the Selfies table, converts it to List of Selfie objects and returns it to the caller.
	 * 
	 * @return List<Selfie>
	 */

	public List<Selfie> getSelfiesList(){
		Log.d(TAG,"getSelfiesList() called");
		Cursor cursor =  mContext.getContentResolver().query(SelfieEntry.CONTENT_URI, null, null, null, null);
		List<Selfie> list = new ArrayList<Selfie>();
			if (cursor.moveToFirst()){
				do {Selfie selfie = new Selfie();
					selfie.setId(cursor.getLong(cursor.getColumnIndex(SelfieEntry._ID)));
					selfie.setTitle(cursor.getString(cursor.getColumnIndex(SelfieEntry.COLUMN_TITLE)));
					selfie.setDate(cursor.getString(cursor.getColumnIndex(SelfieEntry.COLUMN_DATE)));
					selfie.setWidth(cursor.getLong(cursor.getColumnIndex(SelfieEntry.COLUMN_WIDTH)));
					selfie.setHeight(cursor.getLong(cursor.getColumnIndex(SelfieEntry.COLUMN_HEIGHT)));
					selfie.setFileLocation(cursor.getString(cursor.getColumnIndex(SelfieEntry.COLUMN_FILE_LOCATION)));	
					selfie.setThumbnail(ImageUtils.getPhoto(cursor.getBlob(cursor.getColumnIndex(SelfieEntry.COLUMN_THUMBNAIL))));
					
					list.add(selfie);
					
				} while (cursor.moveToNext());	
				
			} else 
				return null;
			
			if (!list.isEmpty()){
				return list;
			}
			return null;
	}
	
	public Uri addSelfie (String path){
		Log.d(TAG,"addSelfie() called");
		Selfie selfie = ImageUtils.createSelfie(path);
		
		ContentValues values = new ContentValues();

		 values.put(SelfieEntry.COLUMN_TITLE,selfie.getTitle());
		 values.put(SelfieEntry.COLUMN_DATE,selfie.getDate());
		 values.put(SelfieEntry.COLUMN_WIDTH,selfie.getWidth());
		 values.put(SelfieEntry.COLUMN_HEIGHT,selfie.getHeight());
		 values.put(SelfieEntry.COLUMN_THUMBNAIL,ImageUtils.getBytes(selfie.getThumbnail()));
		 values.put(SelfieEntry.COLUMN_FILE_LOCATION,selfie.getFileLocation());
		
		 return mContext.getContentResolver().insert(SelfieEntry.CONTENT_URI, values);
		
	}
	
	public int deleteSelfies(){
		return mContext.getContentResolver().delete(SelfieEntry.CONTENT_URI, null, null);
	}

	public int deleteSelfie(Long id) {
			String [] selectionArgs = { Long.toString(id) };
			int deletedRows = mContext.getContentResolver().delete(SelfieEntry.CONTENT_URI, SelfieEntry.SELECTION_SELFIE, selectionArgs );
			Log.d(TAG, selectionArgs[0]);
			Log.d(TAG, Integer.toString(deletedRows));
			return deletedRows;
	}

}
