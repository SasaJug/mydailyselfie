package com.sasaj.mydailyselfie.provider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class SelfieContract {
	  /**
     * The "Content authority" is a name for the entire content
     * provider, similar to the relationship between a domain name and
     * its website. A convenient string to use for the content
     * authority is the package name for the app, which is guaranteed
     * to be unique on the device.
     */
    public static final String CONTENT_AUTHORITY =
        "com.sasaj.selfieprovider";
    
    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which
     * apps will use to contact the content provider.
     */
    public static final Uri BASE_CONTENT_URI =
        Uri.parse("content://"
                  + CONTENT_AUTHORITY);

    /**
     * Possible paths (appended to base content URI for possible URI's) For
     * instance, content://com.sasaj/video/ is a valid path for looking at
     * Acronym data. content://com.sasaj/givemeroot/ will fail, as the
     * ContentProvider hasn't been given any information on what to do with
     * "givemeroot".
     */
    public static final String PATH_SELFIE=
        SelfieEntry.TABLE_NAME; 
    
    
    /**
     * Inner class that defines the table contents of the Video
     * table.
     */
    public static final class SelfieEntry implements BaseColumns {

    	
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Video
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI = 
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_SELFIE).build();
            

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
            "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY 
            + "/" 
            + PATH_SELFIE;

        
        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
            "vnd.android.cursor.item/"
            + CONTENT_AUTHORITY 
            + "/" 
            + PATH_SELFIE;
		

        /**
         * Selection clause to find rows with given title.
         */
     public static final String SELECTION_SELFIE = 
            SelfieEntry._ID
            + " = ?";
        
        
        /**
         * Name of the database table.
         */
		public static final String TABLE_NAME = "selfie_table";

        
        /**
         * Columns to store Data of each Video.
         */
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_THUMBNAIL = "thumbnail";
        public static final String COLUMN_WIDTH= "width";
        public static final String COLUMN_HEIGHT= "height";
        public static final String COLUMN_FILE_LOCATION= "file_location";

        
        /**
         * Return a Uri that points to the row containing a given id.
         * 
         * @param id
         * @return Uri
         */
        public static Uri buildSelfieUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                                              id);
        }

    }  
}
