package com.sasaj.mydailyselfie.provider;

import java.io.File;

import com.sasaj.mydailyselfie.provider.SelfieContract.SelfieEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SelfieDbHelper extends SQLiteOpenHelper {
	
    /**
     * If the database schema is changed, the database version must be
     * incremented.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Database name.
     */
    public static final String DATABASE_NAME =
        "selfie.db";
    

	public SelfieDbHelper(Context context) {
		super(context, context.getCacheDir()
	              + File.separator 
	              + DATABASE_NAME,
	              null, 
	              DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		 // Define an SQL string that creates a table to hold Videos.
        final String SQL_CREATE_VIDEO_TABLE = "CREATE TABLE "
            + SelfieEntry.TABLE_NAME + " (" 
            + SelfieEntry._ID + " INTEGER PRIMARY KEY, " 
            + SelfieEntry.COLUMN_TITLE + " TEXT NOT NULL, " 
            + SelfieEntry.COLUMN_DATE + " TEXT NOT NULL, " 
            + SelfieEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, " 
            + SelfieEntry.COLUMN_WIDTH + " LONG NOT NULL, " 
            + SelfieEntry.COLUMN_HEIGHT + " LONG NOT NULL, " 
            + SelfieEntry.COLUMN_FILE_LOCATION + " TEXT NOT NULL" 
            + " );";
        
        // Create the table.
        db.execSQL(SQL_CREATE_VIDEO_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		   db.execSQL("DROP TABLE IF EXISTS " + SelfieEntry.TABLE_NAME);
		   onCreate(db);
	}

}
