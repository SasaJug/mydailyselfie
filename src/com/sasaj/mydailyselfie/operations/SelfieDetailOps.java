package com.sasaj.mydailyselfie.operations;

import java.io.File;
import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.sasaj.mydailyselfie.R;
import com.sasaj.mydailyselfie.activities.ShowSelfieActivity;
import com.sasaj.mydailyselfie.provider.SelfieController;
import com.sasaj.mydailyselfie.utils.ConfigurableOps;
import com.sasaj.mydailyselfie.utils.Utils;

public class SelfieDetailOps  implements ConfigurableOps {
	
	private final String TAG = getClass().getSimpleName();

	private WeakReference<ShowSelfieActivity> mActivity;
	  
	private ImageView mSelfie;

	private Context mApplicationContext;

	private String mCurrentPath;

	private long mCurrentId;

	private Intent intent;
	

	@Override
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		
        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>((ShowSelfieActivity) activity);
        
        // Get reference to the ImageView for displaying the Image
        mSelfie = (ImageView) mActivity.get().findViewById(R.id.img_selfie);
        
        if (firstTimeIn){
            mApplicationContext = activity.getApplicationContext();
            
        	intent = mActivity.get().getIntent();
    		mCurrentPath = intent.getExtras().getString(mActivity.get().LOCATION);
    		mCurrentId = intent.getExtras().getLong(mActivity.get().ID);
    		Log.d(TAG, mCurrentPath); 
    		Log.d(TAG, Long.toString(mCurrentId));
        }
        
        showImage(mCurrentPath);

	}
	

	public void showImage(String path) {
		displayImage(BitmapFactory.decodeFile(path));
		
	}

	public void deleteSelfie() {

		
		DeleteSelfieFromdb deleteTask = new DeleteSelfieFromdb();
		deleteTask.execute(mCurrentId);
		
	}

	public void filterSelfie() {
		// TODO Auto-generated method stub
		
	}
	
	
	public void finish(){
		mActivity.get().setResult(mActivity.get().RESULT_OK, intent);
		mActivity.get().finish();
	}
	
    void displayImage(Bitmap image)
    {   
        if (mSelfie == null)
            Utils.showToast(mApplicationContext, "Problem with Application,"
                           + " please contact the Developer.");
        else if (image != null)
        	mSelfie.setImageBitmap(image);
        else
            Utils.showToast(mApplicationContext, "image is corrupted,"
                           + " please check the requested URL.");
    }
    

	public class DeleteSelfieFromdb extends AsyncTask <Long, Void, Integer>{

		@Override
		protected Integer doInBackground(Long... params) {
			Log.d(TAG,"doInBackground called");
			
			try{
				 
	    		File file = new File(mCurrentPath);
	 
	    		if(file.delete()){
	    			System.out.println(file.getName() + " is deleted!");
	    		}else{
	    			System.out.println("Delete operation is failed.");
	    		}
	 
	    	}catch(Exception e){
	 
	    		e.printStackTrace();
	    	}
			SelfieController controller = new SelfieController(mApplicationContext);
			return controller.deleteSelfie(params[0]);
		}
		

		@Override
		protected void onPostExecute(Integer result) {
			
			finish();
		}


	}


}
