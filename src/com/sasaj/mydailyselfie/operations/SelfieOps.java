package com.sasaj.mydailyselfie.operations;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.sasaj.mydailyselfie.R;
import com.sasaj.mydailyselfie.activities.SelfieListActivity;
import com.sasaj.mydailyselfie.activities.ShowSelfieActivity;
import com.sasaj.mydailyselfie.model.Selfie;
import com.sasaj.mydailyselfie.provider.SelfieController;
import com.sasaj.mydailyselfie.utils.ConfigurableOps;
import com.sasaj.mydailyselfie.utils.SelfieListAdapter;
import com.sasaj.mydailyselfie.utils.Utils;


public class SelfieOps implements ConfigurableOps {
	
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();
    
	
	public static final int REQUEST_IMAGE_CAPTURE = 1;
	public static final int REQUEST_TAKE_PHOTO = 0;
	public static final int REQUEST_SHOW_PHOTO = 2;
	
	private String mCurrentPhotoPath;
   
    /**
     * Used to enable garbage collection.
     */
    private WeakReference<SelfieListActivity> mActivity;

	private ListView mSelfieList;
	
	private Context mApplicationContext;
	   
    /**
     * Keeps track of list of selfies in the db.
     */
	private List<Selfie> selfiesList;


	private SelfieListAdapter mAdapter;


	/**
     * Default constructor that's needed by the GenericActivity
     * framework.
     */
    public SelfieOps() {
    }
    

	@Override
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d(TAG,
              "onConfiguration() called the "
              + time
              + " with activity = "
              + activity);

        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>((SelfieListActivity) activity);
        // Get reference to the ListView for displaying the results
        // entered.
        mSelfieList = (ListView) mActivity.get().findViewById(R.id.SelfiesList);
        
        
        // Set empty view to show when list is empty.
        mSelfieList.setEmptyView(mActivity.get().findViewById(R.id.empty));
        

        mSelfieList.setOnItemClickListener(new OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mApplicationContext, ShowSelfieActivity.class);
				Selfie selfie = (Selfie) mAdapter.getItem(position);
				Log.d(TAG, selfie.toString());
				intent.putExtra(ShowSelfieActivity.LOCATION, selfie.getFileLocation());
				intent.putExtra(ShowSelfieActivity.ID, selfie.getId());
				mActivity.get().startActivityForResult(intent, REQUEST_SHOW_PHOTO);

    		}
    	});
        
        if (firstTimeIn) {
        	
            mApplicationContext = activity.getApplicationContext();
            
            // Create a local instance of our custom Adapter for ListView.
            mAdapter = new SelfieListAdapter(mApplicationContext);
            

            getList();
        }

        mSelfieList.setAdapter(mAdapter);
	}    


	public void dispatchTakePictureIntent() {
		Log.i(TAG, "entered dispatchTakePictureIntent()");
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(mActivity.get().getPackageManager()) != null) {
	    	File photoFile = null;
	        try {
	            photoFile = createImageFile();
	        } catch (IOException ex) {
	            // Error occurred while creating the File
	        	Log.e(TAG, "file not created");
	        }
	        // Continue only if the File was successfully created
	        if (photoFile != null) {
	            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
	            
		            mActivity.get().startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
	            

	            Log.i(TAG, "photo activity started");
	        }
	    }
	}

	
	
	private File createImageFile() throws IOException {
	    // Create an image file name
		
	    String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss",Locale.ENGLISH).format(new Date());
	    String imageFileName = "DS_" + timeStamp + "_";
	    File storageDir = Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES);
	    Log.i(TAG, storageDir.toString() + imageFileName.toString() + timeStamp.toString());
	    File image = File.createTempFile(
	        imageFileName,  /* prefix */
	        ".jpg",         /* suffix */
	        storageDir      /* directory */
	    );
	    

	    // Save a file: path for use with ACTION_VIEW intents
	    mCurrentPhotoPath = image.getAbsolutePath();
	    
	    return image;
	}
	
	
	public void saveSelfie (){
		SaveSelfieToDb saveSelfieToDbTask = new SaveSelfieToDb();
		saveSelfieToDbTask.execute(mCurrentPhotoPath);
	}
	
	public void getList(){
		GetSelfiesList getSelfiesListTask = new GetSelfiesList();
		getSelfiesListTask.execute();
	}
	
    /**
     * Display the Videos in ListView
     * 
     * @param videos
     */
    public void displaySelfieList(List<Selfie> selfies) {
        if (selfies != null) {
            	mAdapter.setSelfiesList(selfies);    	
        } else {
        	mAdapter.setSelfiesList(new ArrayList<Selfie>());
            Utils.showToast(mActivity.get(),"List is empty");

        }

    }


	public class SaveSelfieToDb extends AsyncTask <String, Void, Uri>{

		@Override
		protected Uri doInBackground(String... params) {
			Log.d(TAG,"doInBackground called");
			SelfieController controller = new SelfieController(mApplicationContext);
			return controller.addSelfie(params[0]);
		}
		

		@Override
		protected void onPostExecute(Uri result) {
			
			getList();
		}


	}
	

	public class GetSelfiesList extends AsyncTask <Void, Void, List<Selfie>>{


		@Override
		protected List<Selfie> doInBackground(Void... params) {
			Log.d(TAG,"doInBackground called");
			SelfieController controller = new SelfieController(mApplicationContext);
			return controller.getSelfiesList();
		}

		@Override
		protected void onPostExecute(List<Selfie> result) {
			displaySelfieList(result);
		}
	}


}
