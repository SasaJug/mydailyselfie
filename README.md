# README #


### MyDailySelfie ###

* This app enables user to take selfies with phone camera. Taken selfies are listed in starting activity. Click on camera icon opens device's camera. Click on list item opens show selfie activity.
* The app is not published on GooglePlay. This is improved version of app developed as a final project for [Programming Mobile Applications for Android Handheld Systems: Part 2 MOOC on Coursera](https://www.coursera.org/course/androidpart2).

* [YouTube screencast](https://youtu.be/bLC6yMLjy3Y)

* Left to be implemented:

1. Alarm (for taking one selfie daily)
2. Grayscale filter

### Features ###

* Configuration changes are handled by GenericActivity/RetainFragmentManager/ConfigurableOps frameworks.
* Selfie metadata are saved in database. ContentProvider is also implemented.